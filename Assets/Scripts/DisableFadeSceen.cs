﻿using UnityEngine;
using System.Collections;

public class DisableFadeSceen : MonoBehaviour
{
    public void Disable()
    {
        gameObject.SetActive(false);
    }
}
