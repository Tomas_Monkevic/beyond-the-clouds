﻿using UnityEngine;

public class Island : MonoBehaviour
{
    public static float referenceWidth; //should be set to players local scale
    public static Color referenceColor; //?????????????????
    public enum IslandType
    {
        normal,
        healing
    }

    public IslandType type;
    public AudioClip nailItSound;
    public Color color;
    public float width;
    public float randomSpeedBoost;
    public float speed = 5f; //randomize a lil bit
    [Range(0,1)]
    public float chanceOfSpawn;
    
    [HideInInspector]
    public float _maxHorizontal;
    private bool _movmentDirectionRight;
    public static float _speedIncreaseOverScore = 1f;

    public virtual void Start()
    {
        GetComponent<SpriteRenderer>().size = new Vector2(referenceWidth, 1);
        UpdateMaxHorizontalPositions();
        _movmentDirectionRight = RandomDirection();
        float rand = Random.Range(0f, randomSpeedBoost);
        transform.position = new Vector3(Random.Range(-1*_maxHorizontal, _maxHorizontal), transform.position.y, transform.position.z);
        //Debug.Log(rand);
        speed += rand;
        SetIslandColor(ColorManager.baseBgColor.MergeColors(ColorManager.cloudColor));
    }

    public virtual void FixedUpdate()
    {
        Move();
	}

    public void UpdateMaxHorizontalPositions()
    {
        _maxHorizontal = Camera.main.ViewportToWorldPoint(new Vector3(1, transform.position.y, 0)).x - referenceWidth/2;
    }

    public bool RandomDirection()
    {
        return Random.value <= 0.5f ? true : false;
    }

    public void Move()
    {
        if (_movmentDirectionRight)
        {
            transform.position += PositionChange();
        }
        else
        {
            transform.position -= PositionChange();
        }
        if (transform.position.x >= _maxHorizontal) //add this to a seperate function
        {
            _movmentDirectionRight = false;
        }
        else if (transform.position.x <= -1 * _maxHorizontal)
        {
            _movmentDirectionRight = true;
        }
    }

    private Vector3 PositionChange()
    {
        return Vector3.right * speed * _speedIncreaseOverScore * Time.deltaTime;
    }

    public void SetIslandColor(Color islandColor)
    {
        GetComponent<SpriteRenderer>().color = islandColor;
    }

    public virtual void Special(GameObject player)
    {
    }
}
