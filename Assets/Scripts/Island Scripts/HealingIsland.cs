﻿using UnityEngine;
using System.Collections;

public class HealingIsland : Island
{
    public float regenAmount;

    private Transform rightTrail;
    private Transform leftTrail;

    public override void Start()
    {
        base.Start();
        rightTrail = transform.GetChild(0);
        leftTrail = transform.GetChild(1);
        SetIslandColor(base.color); //should be changed later
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
        TrailControl(); //not ompitemal to update trails evry frame
    }

    public override void Special(GameObject player)
    {
        AddWidth(gameObject, player, regenAmount);
    }

    public static void AddWidth(GameObject from, GameObject obj, float regenAmount)
    {
        obj.transform.SetParent(null);
        //Debug.Log(player.transform.localScale.x);
        //Debug.Log(player.GetComponent<Player>().width);
        float regen = Mathf.Clamp(obj.GetComponent<SpriteRenderer>().size.x + regenAmount, 0, obj.GetComponent<Player>().width);
        from.GetComponent<SpriteRenderer>().size = new Vector2(regen, 1);
        obj.GetComponent<SpriteRenderer>().size = new Vector2(regen, 1);
        Island.referenceWidth = regen;
        from.GetComponent<Island>().UpdateMaxHorizontalPositions();
        SpawnedIslands.ChangeIslandsSizes(from.GetComponent<Island>());
        //Debug.Log(player.transform.localScale.x);
    }

    public void TrailControl()
    {
        rightTrail.localPosition = new Vector2(Island.referenceWidth / 2.0f, 0);
        leftTrail.localPosition = new Vector2(-Island.referenceWidth / 2.0f, 0); ;
        //set trail color as island color, maybe a litlle bit lither
        //if (transform.position.x >= _maxHorizontal)
        //{
        //    rightTrail.gameObject.SetActive(true);
        //    leftTrail.gameObject.SetActive(false);
        //}
        //else if (transform.position.x <= -1 * _maxHorizontal)
        //{
        //    rightTrail.gameObject.SetActive(false);
        //    leftTrail.gameObject.SetActive(true);
        //}
    }
}
