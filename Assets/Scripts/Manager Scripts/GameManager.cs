﻿using UnityEngine;
using UnityEngine.Advertisements;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
    public float jumpErrorTolarance;
    public float speedInc;
    public IslandManager islandManager;
    public CameraMovement cameraMovement;
    public Player player;
    public float regenAmountAfterDeath;

    [HideInInspector]
    public bool isSencondChanceUsed = false;
    private float _scoreDiff;

    private void Awake()
    {
        MenuManager.adWatch = Revive;
        MenuManager.instance._myScore.ResetCurrentScore();
        PassReferences();
        ResetStaticVariables();
        //Debug.Log(Application.persistentDataPath);
    }

    private void PassReferences()
    {
        cameraMovement.gap = islandManager.spawnDensity;
        player.jumpDistance = islandManager.spawnDensity;
        islandManager.initialPosition = player.transform.position;
        Island.referenceWidth = player.width;
    }

    public void GameOver()
    {
        player.GetComponent<Rigidbody2D>().isKinematic = false; //make player fall, but stop the fall evntually!!!!
        Player.isDead = true;
        FileManager.Save<Score>(MenuManager.instance._myScore, "Score.dat");
        //Debug.Log(MenuManager.instance._myScore.Best - MenuManager.instance._myScore.Current);
        _scoreDiff = Mathf.Ceil(MenuManager.instance._myScore.Best / 2.0f);
        if ((MenuManager.instance._myScore.Best - MenuManager.instance._myScore.Current < _scoreDiff ||
            MenuManager.instance._myScore.IsNewBest)
            && !isSencondChanceUsed && Advertisement.IsReady()) 
        {
            MenuManager.instance.JumpsLeftText(MenuManager.instance._myScore.Best - MenuManager.instance._myScore.Current + 1);
            MenuManager.instance.SecondChanceScreen(true);
        }
        else
        {
            MenuManager.instance.EnableGameOverScreen();
        }
        //ResetStaticVariables();
    }

    private void Revive()
    {
        player.GetComponent<Animator>().enabled = true; //for now it works should be changed later
        Player.isDead = false;
        isSencondChanceUsed = true;
        //some effect would be nice
        //player.transform.localPosition = player.prevPossition;
        player.transform.localPosition = Vector3.zero;
        //player.transform.localScale = player.prevWidth;
        Transform parentIsland = player.transform.parent;
        HealingIsland.AddWidth(parentIsland.gameObject, player.gameObject, regenAmountAfterDeath);
        player.transform.SetParent(parentIsland);
        MenuManager.instance.SecondChanceScreen(false);
    }

    private void ResetStaticVariables()
    {
        Player.ResetPlayerJump();
        Player.isDead = false;
        Island.referenceWidth = player.width;
        Island._speedIncreaseOverScore = 1;
        SpawnedIslands.spawnedIslands = new List<Island>();
    }
}
