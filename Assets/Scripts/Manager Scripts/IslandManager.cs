﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class IslandManager : MonoBehaviour
{
    public List<Island> islands = new List<Island>();
    [Range(1f, 10f)]
    public float spawnDensity = 2f;

    [HideInInspector]
    public Vector3 initialPosition;

    private Vector3 _density;
    private Vector2 _viewPortPosition;
    private Vector3 _spawnPosition;

    private void Start ()
    {
        Player.playerJump += Spawn;
        //Player.playerJump += Despawn;
        Player.playerJump += DespawnNew;
        InitialSpawn();
    }

    private void InitialSpawn()
    {
        _density = new Vector3(0, spawnDensity, 0);
        initialPosition += _density;
        _spawnPosition = initialPosition;
        _viewPortPosition = Vector2.zero;
        while(_viewPortPosition.y <= 1)
        {
            Spawn();
            _viewPortPosition = Camera.main.WorldToViewportPoint(_spawnPosition);
        }
    }

    private void Spawn()
    {
        Island.IslandType islandType = Island.IslandType.normal;
        try
        {
            islandType = RandomIsland();
        }
        catch (System.Exception ex)
        {
            Debug.LogError(ex.Message);
        }
        SpawnedIslands.spawnedIslands.Add(((GameObject)Instantiate(islands[(int)islandType].gameObject, _spawnPosition, Quaternion.identity, transform)).GetComponent<Island>());
        _spawnPosition += _density;
    }

    private void Despawn() //might be changed to a for because curretly it doesn't despawn all islands
    {
        if (Camera.main.WorldToViewportPoint(SpawnedIslands.spawnedIslands[0].transform.position).y < 0) //if island is not visible
        {
            Destroy(SpawnedIslands.spawnedIslands[0]); //destroy it
            SpawnedIslands.spawnedIslands.RemoveAt(0); //and remove from list
        }
    }

    private void DespawnNew()
    {
        List<Island> notVisibleIslands = SpawnedIslands.spawnedIslands.Where(isl => Camera.main.WorldToViewportPoint(isl.transform.position).y < 0).ToList();
        foreach(Island isl in notVisibleIslands)
        {
            //Debug.Log(isl);
            SpawnedIslands.spawnedIslands.Remove(isl);
            Destroy(isl.gameObject);
        }
    }

    private Island.IslandType RandomIsland()
    {
        //check if all chances add up to 1;
        if (ChacesAreValid())
        {
            float randomFloat = Random.value;
            float chance = 0;
            foreach (Island isl in islands)
            {
                if (chance <= randomFloat && randomFloat <= chance + isl.chanceOfSpawn)
                {
                    return isl.type;
                }
                chance += isl.chanceOfSpawn;
            }
        }
        throw new System.Exception("Chances on players are set incorrectly. All chaces shoul add up to 1");
    }

    private bool ChacesAreValid()
    {
        float chance = 0;
        foreach (Island isl in islands)
        {
            chance += isl.chanceOfSpawn;
        }
        return chance == 1 ? true : false;
    }
}
