﻿using UnityEngine;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;

    public List<AudioClip> menuSounds = new List<AudioClip>();

    private AudioSource _gameAudio;

	private void Awake()
    {
        if(instance == null)
        {
            _gameAudio = GetComponent<AudioSource>();
            _gameAudio.mute = GameSettingsContainer.instance.gameSettings.isAudioMuted;
            PlaySound(menuSounds[0]);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
	}

    public void PlaySound(AudioClip sound)
    {
        _gameAudio.Stop();
        _gameAudio.clip = sound;
        _gameAudio.Play();
    }

    public void Mute(bool mute)
    {
        _gameAudio.mute = mute;
        FileManager.Save<GameSettings>(GameSettingsContainer.instance.gameSettings, "Settings.dat");
    }

    public void PlayButtonSound()
    {
        PlaySound(menuSounds[1]);
    }
}
