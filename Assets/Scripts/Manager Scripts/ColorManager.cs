﻿using UnityEngine;

public class ColorManager : MonoBehaviour
{
    public static Color baseBgColor;
    public static Vector3 cloudColor = new Vector3(200, 117, 175);
    public static Vector3 skyColor = new Vector3(64, 80, 138);

    public float colorChangeSpeed = 2f;
    [Range(0,255)]
    public float saturation = 145f;
    [Range(0, 255)]
    public float value = 142f;

    private float _hue;
    private float _t;
    private float _min = 0;
    private float _max = 1;

    private void Awake()
    {
        _t = Random.value;
        ChangeColor();
        Player.playerJump += ChangeColor;
    }

    private void ChangeColor()
    {
        _hue = Mathf.Lerp(_min, _max, _t);
        //Debug.Log(Time.deltaTime);
        _t += 0.01f * colorChangeSpeed;
        if (_t > 1.0f)
        {
            float temp = _max;
            _max = _min;
            _min = temp;
            _t = 0.0f;
        }
        baseBgColor = Color.HSVToRGB(_hue, saturation / 256f, value / 256f);
        RenderSettings.skybox.SetColor("_Tint", baseBgColor);
    }

    public static Color MergeColorWithBackground(float r = 200, float g = 117, float b = 175)
    {
        return new Color((ColorManager.baseBgColor.r * 256 + r) / 2 / 256, (ColorManager.baseBgColor.g * 256 + g) / 2 / 256, (ColorManager.baseBgColor.b * 256 + b) / 2 / 256);
    }
}
