﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Advertisements;

//loading ui scene seems to be not good. maybe change to loading prefabs?
//why the fuck isn't awake called when scene is loaded addative???
//why the fuck should I set instace of score????
//check excecution order

public class MenuManager : MonoBehaviour
{
    public static MenuManager instance;
    public delegate void Action(); //needs to be optimized
    public static Action adWatch;

    public Sprite[] muteButtonsIcons = new Sprite[2];
    public float confirmationTime;

    private GameObject fadeScreen;
    private GameObject gameOverPanel;
    private GameObject rewardScreen;
    private GameObject confirmPanel;
    private GameObject muteButton;
    private GameObject tutorialPanel;
    private Text nJumpsText;
    private Text scoreText;
    private Text bestScoreText;

    public Score _myScore; //place score in game settings?
    private float _timer = 0;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            try //probobly should be moved to game settings
            {
                instance._myScore = FileManager.Load<Score>("Score.dat");
            }
            catch //if something bad happens make a new score
            {
                instance._myScore = new Score();
            }
            StartCoroutine(LoadUI());
            SceneManager.sceneLoaded += IsLoadUI;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        //Player.playerJump += UpdateScore;
    }

    private void Update()
    {
        if (_timer > 0)
        {
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                _timer = 0;
                //Debug.Log("Second");
                GoBack();
            }
            _timer -= Time.deltaTime;
            if (_timer <= 0)
            {
                confirmPanel.SetActive(false);
                _timer = 0;
            }
        }
        else if (Input.GetKeyUp(KeyCode.Escape))
        {
            //Debug.Log("First");
            EnableConfirmation();
        }
    }

    private void EnableConfirmation()
    {
        confirmPanel.SetActive(true);
        _timer = confirmationTime;
    }

    private void IsLoadUI(Scene newScene, LoadSceneMode loadMode)
    {
        if(LoadSceneMode.Single == loadMode)
        {
            //Debug.Log("Scene was loaded");
            StartCoroutine(LoadUI());
        }
        else if (LoadSceneMode.Additive == loadMode)
        {
            //Debug.Log("Scene was loaded addative");
            
        }
    }

    IEnumerator LoadUI()
    {
        Scene activeScene = SceneManager.GetActiveScene();
        string loadedSceneName = "";
        switch(activeScene.name)
        {
            case "Main Menu":
                SceneManager.LoadScene("UI - Main Menu", LoadSceneMode.Additive);
                loadedSceneName = "UI - Main Menu";
                break;
            case "Level":
                loadedSceneName = "UI - Level";
                SceneManager.LoadScene("UI - Level", LoadSceneMode.Additive);
                break;
        }
        //wait until end of frame?
        //no need for wait?
        //yield return new WaitForEndOfFrame();
        yield return 0;
        SetReferences(SceneManager.GetSceneByName(loadedSceneName));
        ChangeMuteButtonSprite();
        //EnableFade();
    }

    private void SetReferences(Scene scene) //need to think about this funcntion
    {
        //create a function that get index from name (where ever it is)
        GameObject uiObj = scene.GetRootGameObjects()[0];
        int uiChildCount = uiObj.transform.childCount;
        fadeScreen = uiObj.transform.GetChild(uiChildCount - 1).gameObject;
        confirmPanel = uiObj.transform.GetChild(uiChildCount - 2).gameObject;
        if(scene.name == "UI - Main Menu")
        {
            bestScoreText = uiObj.transform.Find("Best Score").GetComponent<Text>();
            bestScoreText.text = "Best Score: " + instance._myScore.Best;
            uiObj.transform.Find("Play").gameObject.GetComponent<Button>().onClick.AddListener(() => { LoadScene(1); });
            muteButton = uiObj.transform.Find("Options").Find("muteButton").gameObject;
            muteButton.GetComponent<Button>().onClick.AddListener(() => { MuteAudio(); });
            uiObj.transform.Find("Options").Find("rateButton").GetComponent<Button>().onClick.AddListener(() => { Rate(); } );
        }
        else if(scene.name == "UI - Level")
        {
            rewardScreen = uiObj.transform.Find("Reward Screen").gameObject;
            nJumpsText = rewardScreen.transform.Find("N Left").GetComponent<Text>();
            Transform buttonsPanel = rewardScreen.transform.Find("Button Container");
            buttonsPanel.transform.Find("RestartButton").GetComponent<Button>().onClick.AddListener(() => { LoadScene(1); });
            buttonsPanel.transform.Find("Ad Button").GetComponent<Button>().onClick.AddListener(() => { ShowRewardedAd(); });
            //---------------------
            scoreText = uiObj.transform.Find("Score").GetComponent<Text>();
            gameOverPanel = uiObj.transform.Find("Game Over screen").gameObject;
            bestScoreText = gameOverPanel.transform.Find("Best Score").GetComponent<Text>();
            gameOverPanel.transform.Find("RestartButton").GetComponent<Button>().onClick.AddListener(() => { LoadScene(1); });
            muteButton = gameOverPanel.transform.Find("Options").Find("muteButton").gameObject;
            gameOverPanel.transform.Find("Options").Find("rateButton").GetComponent<Button>().onClick.AddListener(() => { Rate(); });
            muteButton.GetComponent<Button>().onClick.AddListener(() => { MuteAudio(); });
            if(GameSettingsContainer.instance.gameSettings.isFirsTimeLaunched)
            {
                tutorialPanel = uiObj.transform.Find("Tutorial").gameObject;
                TutorialPanel(true);
            }
        }
    }

    private void GoBack()
    {
        int newSceneIndex = SceneManager.GetActiveScene().buildIndex - 1;
        if (newSceneIndex < 0)
        {
            #if !UNITY_EDITOR
                Application.Quit();
            #else
                UnityEditor.EditorApplication.isPlaying = false;
            #endif
        }
        else
        {
            LoadScene(newSceneIndex);
        }
    }

    public void ShowRewardedAd()
    {
        if (Advertisement.IsReady("rewardedVideo"))
        {
            var options = new ShowOptions { resultCallback = HandleShowResult };
            float time = Time.timeScale;
            Time.timeScale = 0;
            Advertisement.Show("rewardedVideo", options);
            Time.timeScale = time;
        }
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                //Debug.Log("The ad was successfully shown.");
                if(adWatch != null)
                {
                    adWatch();
                }
                break;
            case ShowResult.Skipped:
                //Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                //Debug.LogError("The ad failed to be shown.");
                break;
        }
    }

    private void Rate()
    {
        Application.OpenURL("market://details?id=com.TomasMonkevic.BeyondTheClouds");
    }

    private void ChangeMuteButtonSprite()
    {
        instance.muteButton.GetComponent<Image>().sprite = GameSettingsContainer.instance.gameSettings.isAudioMuted ? muteButtonsIcons[1] : muteButtonsIcons[0];
    }

    public void UpdateScore()
    {
        if(!Player.isDead)
        {
            instance._myScore.IncreaseScore();
            instance.scoreText.text = instance._myScore.Current.ToString();
        }
    }

    public void JumpsLeftText(int n)
    {
        nJumpsText.text = !_myScore.IsNewBest ? "Only " + n + " jumps left" : "Improve your score even more!";
    }

    public void MuteAudio()
    {
        GameSettingsContainer.instance.gameSettings.isAudioMuted = !GameSettingsContainer.instance.gameSettings.isAudioMuted;
        AudioManager.instance.Mute(GameSettingsContainer.instance.gameSettings.isAudioMuted);
        ChangeMuteButtonSprite();
    }

    public void LoadScene(int sceneIndex)
    {
        if(sceneIndex == 1)
        {
            AudioManager.instance.PlayButtonSound();
        }
        SceneManager.LoadScene(sceneIndex);
    }

    public void EnableGameOverScreen()
    {
        gameOverPanel.SetActive(true);
        string bsText = string.Empty;
        if(instance._myScore.IsNewBest) //only < ?
        {
            bsText = "New ";
            //add some cool PS effects or audio
        }
        bsText += "Best Score: " + instance._myScore.Best;
        instance.bestScoreText.text = bsText;
    }

    public void EnableFade()
    {
        fadeScreen.SetActive(true);
    }

    public void TutorialPanel(bool activete)
    {
        tutorialPanel.SetActive(activete);
    }

    public void SecondChanceScreen(bool activate)
    {
        rewardScreen.SetActive(activate);
    }
}
