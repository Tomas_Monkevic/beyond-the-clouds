﻿using UnityEngine;
using System.Collections;

public struct Cutoff //cutoff isn't deleted after spawn
{
    public static float destroyAfter = 1f;

    private GameObject _prefab;
    private Color _color;
    private float _width;
    private Vector3 _position;

    public Cutoff(GameObject prefab, Color col, float width, Vector3 pos)
    {
        _prefab = prefab;
        _color = col;
        _width = width;
        _position = pos;
    }

    public void Spawn()
    {
        GameObject cutoff = MonoBehaviour.Instantiate(_prefab, _position, Quaternion.identity);
        cutoff.GetComponent<BoxCollider2D>().size = new Vector2(_width, 1.0f);
        cutoff.GetComponent<SpriteRenderer>().color = _color;
        cutoff.GetComponent<SpriteRenderer>().size = new Vector2(_width, 1.0f);
        MonoBehaviour.Destroy(cutoff, destroyAfter);
    }
}
