﻿using UnityEngine;

public class Player : MonoBehaviour
{
    public delegate void JumpDelegate ();

    public static event JumpDelegate playerJump;

    public GameManager gameManager;
    public GameObject cutOffPrefab;
    public AudioClip cutOffSound;
    public float width = 1.5f;
    public float minPlayerWidth = 0.05f;
    public GameObject perfectJumpEffect;

    [HideInInspector]
    public float jumpDistance;
    [HideInInspector]
    public Vector3 prevPossition = Vector3.zero;
    [HideInInspector]
    public Vector3 prevWidth;

    public static bool isDead = false;

    private Vector3 _nextPosition;
    private Island _nextIsland;
    private float _offset;
    private SpriteRenderer _spriteRenderer;

    private void Start()
    {
        playerJump += MenuManager.instance.UpdateScore;
        GetComponent<SpriteRenderer>().size = new Vector2(width, 1);
        _nextPosition = transform.position;
        _spriteRenderer = GetComponent<SpriteRenderer>();
        SetSpriteColor();
    }

    private void Update ()
    {
        if(!isDead && (Input.GetButtonDown("Fire1") || Input.GetKeyDown(KeyCode.Space)))
        {
            Jump();
            if (GameSettingsContainer.instance.gameSettings.isFirsTimeLaunched)
            {
                MenuManager.instance.TutorialPanel(false);
                GameSettingsContainer.instance.gameSettings.isFirsTimeLaunched = false;
                FileManager.Save<GameSettings>(GameSettingsContainer.instance.gameSettings, "Settings.dat"); //move to gameSettingsContainer?
            }
        }
        if(Camera.main.WorldToViewportPoint(transform.position).y < -1)
        {
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            GetComponent<Rigidbody2D>().isKinematic = true;
        }
        //Debug.Log("Posiiton: " + transform.position + "Local Position: " + transform.localPosition);
        //Debug.Log("Local Scale: " + transform.localScale + "Lossy Scale: " + transform.lossyScale);
    }

    private void Jump()
    {
        prevPossition = transform.parent == null ? Vector3.zero : transform.localPosition;
        prevWidth = GetComponent<SpriteRenderer>().size;
        //Debug.Log("Position: " + prevPossition);
        //Debug.Log("Postion: " + prevWidth);
        //Debug.Log("Jump!");
        _nextPosition = new Vector3(transform.position.x, transform.position.y + jumpDistance, 0);
        //Debug.Log(nextPosition);
        try
        {
            _nextIsland = SpawnedIslands.GetIslandByPosition(_nextPosition);
        }
        catch (System.Exception ex)
        {
            Debug.LogError(ex.Message);
            return;
        }
        _offset = _nextIsland.transform.position.x - transform.position.x;
        //Debug.Log(_offset);
        Island._speedIncreaseOverScore += gameManager.speedInc;
        if (Mathf.Abs(_offset) <= gameManager.jumpErrorTolarance) //next position doesn't work?
        {
            PerfectJump();
        }
        else
        {
            transform.position = _nextPosition;
            if (Mathf.Abs(_offset) > GetComponent<SpriteRenderer>().size.x * 2 || GetComponent<SpriteRenderer>().size.x - Mathf.Abs(_offset) <= minPlayerWidth)
            {
                AudioManager.instance.PlaySound(cutOffSound);
                gameManager.GameOver();
            }
            else
            {
                CutOff();
            }
        }
        SetSpriteColor();
        ChangeColliderSize();
        if (playerJump != null)
        {
            playerJump();
        }
        //playerJump?.Invoke(); :(
    }

    private void PerfectJump()
    {
        AudioManager.instance.PlaySound(_nextIsland.nailItSound);
        _nextIsland.Special(gameObject);
        _offset = 0;
        transform.position = _nextIsland.transform.position; //add to normal special????

        GameObject effect = Instantiate(perfectJumpEffect, transform.position, Quaternion.identity);
        ParticleSystem ps = effect.GetComponent<ParticleSystem>();
        var main = ps.main;
        main.startColor = ColorManager.baseBgColor.MergeColors(ColorManager.skyColor);
        ps.Play();
        Destroy(ps.gameObject, ps.main.duration);
        transform.SetParent(_nextIsland.transform);
    }

    private void ChangeColliderSize()
    {
        GetComponent<BoxCollider2D>().size = new Vector2(GetComponent<SpriteRenderer>().size.x, 1.0f); //TODO remvoe all hardcoded 1
    }

    private void SetSpriteColor()
    {
        //_spriteRenderer.color = ColorManager.MergeColorWithBackground(r: 64, b: 138, g: 80);
        _spriteRenderer.color = ColorManager.baseBgColor.MergeColors(ColorManager.skyColor);
    }

    private void CutOff()
    {
        //Debug.Log(offset);
        transform.SetParent(null);
        GetComponent<SpriteRenderer>().size -= new Vector2(Mathf.Abs(_offset), 0);
        AudioManager.instance.PlaySound(cutOffSound);
        CreateCutOff();
        transform.SetParent(_nextIsland.transform);
        _nextPosition += new Vector3(_offset / 2, 0, 0);
        transform.position = _nextPosition;
        Island.referenceWidth -= Mathf.Abs(_offset);
        SpawnedIslands.ChangeIslandsSizes(_nextIsland);
    }

    private void CreateCutOff()
    {

        Vector3 pos = _offset <=0 ? _nextIsland.transform.position + new Vector3(Island.referenceWidth/2 + (Mathf.Abs(_offset) / 2), 0, 0) :  _nextIsland.transform.position - new Vector3(Island.referenceWidth/2 + (Mathf.Abs(_offset) / 2), 0, 0);
        Cutoff cutoff = new Cutoff(cutOffPrefab, ColorManager.MergeColorWithBackground(r: 64, b: 138, g: 80), Mathf.Abs(_offset), pos);
        cutoff.Spawn();
    }
    
    public static void ResetPlayerJump()
    {
        playerJump = null;
    }
}
