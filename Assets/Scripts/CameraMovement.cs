﻿using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public float smoothTime = 0.3f;

    [HideInInspector]
    public float gap;

    private Vector3 _velocity = Vector3.zero;
    private Vector3 _nextPosition;

    private void Start()
    {
        Player.playerJump += MoveForward;
        MenuManager.adWatch += MoveBack;
        _nextPosition = transform.position;
    }

    private void FixedUpdate()
    {
        transform.position = Vector3.SmoothDamp(transform.position, _nextPosition, ref _velocity, smoothTime);
    }

    private void MoveForward()
    {
        _nextPosition += new Vector3(0, gap, 0);
    }

    public void MoveBack()
    {
        _nextPosition -= new Vector3(0, gap, 0);
    }
}
