﻿[System.Serializable]
public class Score
{
    public int Current 
    {
        get
        {
            return _current;
        }
    }

    public int Best
    {
        get
        {
            return _best;
        }
    }

    public bool IsNewBest
    {
        get
        {
            return _isNewBest;
        }
    }

    [System.NonSerialized]
    public bool _isNewBest = false; //don't serialize
    [System.NonSerialized]
    private int _current = 0; //don't serialize
    private int _best;

    public void IncreaseScore(int inc = 1)
    {
        _current += inc;
        if (_current > _best)
        {
            _best = _current;
            _isNewBest = true;
        }
    }

    public void ResetCurrentScore()
    {
        _isNewBest = false;
        _current = 0;
    }
}
