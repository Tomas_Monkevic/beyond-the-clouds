﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;

public class GameSettingsContainer : MonoBehaviour
{
    public static GameSettingsContainer instance;

    [HideInInspector]
    public GameSettings gameSettings;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            try
            {
                gameSettings = FileManager.Load<GameSettings>("Settings.dat");
            }
            catch
            {
                gameSettings = new GameSettings();
            }
            
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        //if(gameSettings.showAdOnStartUp)
        //{
        //    StartCoroutine(ShowAdWhenReady());
        //}
        gameSettings.showAdOnStartUp = !gameSettings.showAdOnStartUp;
        FileManager.Save<GameSettings>(gameSettings, "Settings.dat");
    }

    IEnumerator ShowAdWhenReady() //is it bad that it waits for the ad to be ready?
    {
        while (!Advertisement.IsReady())
            yield return null;

        Advertisement.Show ();
    }
}
