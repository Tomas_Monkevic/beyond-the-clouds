﻿using UnityEngine;

//add to vector 3 color in 256 range

public static class ExtentionMethods
{
    public static Color MergeColors(this Color col1, Vector3 col2)
    {
        //return new Color((col1.r + col2.r) / 2, (col1.g + col2.g) / 2, (col1.b + col2.b) / 2); gives interesting results
        return new Color((col1.r * 256 + col2.x) / 2 / 256, (col1.g * 256 + col2.y) / 2 / 256, (col1.b * 256 + col2.z) / 2 / 256);
    }

    public static Color ToColor(this Vector3 vec) //not used yet, but I think will be
    {
        return new Color(vec.x / 256, vec.y / 256, vec.z / 256);
    }

    public static Vector3 ToVector3(this Color col)
    {
        return new Vector3(col.r * 256, col.g * 256, col.b * 256);
    }
}
