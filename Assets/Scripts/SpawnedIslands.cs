﻿using UnityEngine;
using System.Collections.Generic;

public struct SpawnedIslands
{
    public static List<Island> spawnedIslands = new List<Island>();

    public static Island GetIslandByPosition(Vector3 position, float threshold = 0.001f)
    {
        int i = 0;
        while (i < spawnedIslands.Count)
        {
            if (Mathf.Abs(spawnedIslands[i].transform.position.y - position.y) <= threshold)
            {
                return spawnedIslands[i];
            }
            i++;
        }
        throw new System.Exception("Island wasn't found!");
    }

    public static void ChangeIslandsSizes(Island from)
    {
        for (int i = spawnedIslands.IndexOf(from) + 1; i < spawnedIslands.Count; i++)
        {
            spawnedIslands[i].GetComponent<SpriteRenderer>().size = new Vector3(Island.referenceWidth, spawnedIslands[i].GetComponent<SpriteRenderer>().size.y, 1);
            spawnedIslands[i].UpdateMaxHorizontalPositions();
        }
    }
}